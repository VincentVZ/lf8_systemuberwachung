import os
import platform
import datetime
import socket
import sys
import shutil
import psutil
import logging
from time import sleep
import smtplib
import ssl

# Aktuelle Uhrzeit
x = datetime.datetime.now()

h_name = socket.gethostname()



# Warnings werden hier oben erstellt und auf False gesetzt
warningDisk = False
warningCPU = False
warningRAM = False
warningYellow = False
warningRed = False



#Funktion für die Sendung von E-Mail (GMail)
def sendMail(subject, content):
    port = 465
    smtp_server_domain_name = "smtp.gmail.com"
    password = input("Password:")
    ssl_content = ssl.create_default_context()
    service = smtplib.SMTP_SSL(smtp_server_domain_name, port, context=ssl_content)
    service.login("lf8.system@gmail.com", password)
    service.sendmail("lf8.system@gmail.com", "goedecke51@gmail.com", f"Subject:{subject}\n{content}")

#Wird für die Wiederholung benötigt (Alle 10 Sekunden aktualisiert)
while True :

    # Zeigt alle aktuellen warnings an
    def warnings():
        print("Welche Warnungen gibt es derzeitig?")
        print("Warnung zur Festplatte:", warningDisk, "\n", "Warnung zur CPU:", warningCPU, "\n", "Warnung zum RAM:",
              warningRAM, "\n")



    # Funktion für die Trennung zwischen den Systemausgaben
    def trennung():
        print("__________________________________________________", "\n")

    #Begrüßung mit Name, Datum, Uhrzeit, IP-Adresse, Windowsversion
    # Die aktuelle IP-Adresse
    IP_addres = socket.gethostbyname(h_name)

    print("Herzlich willkommen!", "\n", "Moin", os.getlogin(), "Heute ist", x.strftime("%A"), datetime.datetime.now())
    trennung()
    print("Systeminformationen", "\n", "Aktuell angemeldeter Benutzer:", socket.gethostname(), "Sie haben folgende IP-Adresse:", IP_addres, "\n", "Aktuelle Windows Version", platform.platform())
    # Aktuelle Pythonversion
    print("Python Version:", sys.version, "\n", "Aktuelles Verzeichnis: ", os.getcwd(), "\n")



    # Ermittelt den Festplattenspeicher C:/. Zeigt den Gesamten und den Freien Speicher an. Wenn der Freie Speicher unter 20% des Gesamten Speichers ist, wird warningDisk auf True gesetzt.
    # Und es wird eine Nachricht geprinted
    path = 'C:/' # Festplatte C:
    DiskGigaGesamt = shutil.disk_usage(path)[0] /1000/1000/1000 #Umrechnung des Gesamten Speichers von Byte auf GByte
    DiskGigaFrei =  shutil.disk_usage(path)[2] /1000/1000/1000 #Umrechnung des Freien Speichers von Byte auf GByte

    obj_Disk = psutil.disk_usage('/')

    trennung()
    print("Disk-Informationen")                                         #Gesamter Speicher                      #Freier Speicher
    Festplattenspeicher = print("Speicherplatz der Festplatte", "Gesamter Speicher in GByte: ", DiskGigaGesamt, "Freier Speicher in GByte", DiskGigaFrei)
    Festplatte_Prozent = print("Es sind derzeitig", obj_Disk.percent, "% auf der Festplatte", path, "frei")
    if int(obj_Disk.percent) < 20:
        print("Ihr Festplattenspeicher ist zu niedrig! Bitte sorgen Sie für ausreichend Speihcerplatz!")
        warningDisk = True
    else:
        print("Ihr Festplattenspeicher ist genügend.")



    # Ermittelt den Prozessor und die Auslastung der CPU
    trennung()
    CPUAuslastung = 100-psutil.cpu_percent()
    print("CPU-Informationen")
    print("Prozessor-Name und Modell:", platform.processor(), "\n", "Auslastung der CPU in Prozent:", CPUAuslastung)
    if CPUAuslastung < 70.0:
        print("Ihre CPU ist überlastet, schließen sie bitte Programme, damit Ihre CPU nicht überhitzt.")
        warningCPU = True
    else:
        print("Ihre CPU ist nicht überlastet, sie können normal weiter arbeiten.")



    # Ermittelt die Arbeitspeicher Auslastung und setzt RAMAuslastung auf True, wenn die Auslastung über 70% ist
    RAMAuslastung = psutil.virtual_memory().available * 100 / psutil.virtual_memory().total

    trennung()
    print("RAM-Informationen")
    print("Benutzer RAM anteil:", psutil.virtual_memory().percent, "%", "\n", "Restlicher RAM anteil:", RAMAuslastung , "%")
    if RAMAuslastung < 50:
        print("Ihr Arbeitsspeicher ist überlastet. Schließen sie ungenutzte Prozesse und Programme.")
        warningRAM = True
    else:
        print("Ihr Arbeitsspeicher ist nicht überlastet.")



    # AlarmsystemYellow - Wenn von 2 Schwellenwerten eins True ist, wird warningYellow auf True gesetzt und eine Nachricht geprinted
    trennung()
    print("Yellow")
    if warningRAM or warningCPU == True:
        print("Alarmsystem-Yellow")
        warningYellow = True
        print("Achtung, Ihr System hat 1 Schwellenwerte erreicht, bitte nehmen sie folgende Maßnahmen vor:", "\n",
              "-Schließen sie nicht benötigte Programme, die das System überfordern.", "\n",
              "-Schließen sie nicht benötigte Prozesse, die das System überfordern.")
    else:
        print("Ihr System läuft derzeitig ohne Probleme")



    # AlarmsystemRed - Bei Zwei Warnings die True sind, wird warningRed auf True gesetzt und eine Nachricht geprinted
    trennung()
    if warningRAM and warningCPU == True:
        print("Alarmsystem-Red")
        warningRed = True
        print("ACHTUNG, es wurden 2 Schwellenwerte erreicht. Zur Warnung wurde eine E-Mail versendet.")





    # Konfiguration der Log-Datei
    logging.basicConfig(filename="LF8.log", format='%(asctime)s - %(levelname)s - %(message)s ', filemode='w')
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)


    # Es werden verschiedene Informationen in eine Log-Datei gespeichert
    logger.info('Aktuell ist der folgende Nutzer angemeldet: %s' % os.getlogin())
    logger.info('Derzeitig hat die Festplatte C:/ folgende Gigabyte an Festplatttenspeicher frei: %s' % DiskGigaFrei)
    logger.info('Derzeitg hat der Prozessor folgende Prozent an Aulastung frei: %s' % psutil.cpu_percent())
    logger.info('Derzeitg hat der Arbeitsspeicher folgende Prozent an Auslastung erreicht: %s' % psutil.virtual_memory().percent)

    # Wenn das 1. Alarmsystem True ist, wird dies in der Log-Datei gespeichert
    if warningYellow == True:
        logger.warning('')
        logger.warning('Achtung, 1 Schwellenwert wurde erreicht (Warnung: Yellow). Bitte nehmen sie Maßnahmen dagegen vor.')

    # Wenn das 2. Alarmsystem True ist, wird dies in der Log-Datei gespeichert
    if warningRed == True:
        logger.critical("ACHTUNG, es wurden 2 Schwellenwerte erreicht (Warnung: Red). Es wurde eine E-Mail zur Warnung versendet")

    sendMail(f"Systemueberwachung - WarningRed:{warningRed}", f"ACHTUNG, es wurden zwei Schwellenwerte erreicht.\n Der Schwellenwert der CPU ist zu hoch:{CPUAuslastung}. Der Schwellenwert des Arbeitsspeichers ist zu hoch:{RAMAuslastung}")

    # Wenn warningRed true ist, wird eine E-Mail versendet



    #  Nach 10 Sekunden wird das Programm aktualiert
    sleep(10)
